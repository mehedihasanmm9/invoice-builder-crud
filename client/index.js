//const { response } = require("express");
document.addEventListener('DOMContentLoaded', function() {
    fetch('http://localhost:5000/getAll')
    .then(response => response.json())
    .then(data => loadHTMLTable(data['data']))
});

const addOrder = document.querySelector('#add-order-btn')
addOrder.onclick = () => {
    const invoiceNoIn = document.querySelector('#invoice-no')
    const invoiceNo = invoiceNoIn.value //Invoice_No from here
    invoiceNoIn.value = ""

    const customerNameIn = document.querySelector('#customer-name')
    const customerName = customerNameIn.value //Customer_Name from here
    customerNameIn.value = ""

    const productNameIn = document.querySelector('#product-name')
    const productName = productNameIn.value //Product_name from here
    productNameIn.value = ""

    const descriptionIn = document.querySelector('#description')
    const description = descriptionIn.value //Description from here
    descriptionIn.value = ""

    const quantityIn = document.querySelector('#quantity')
    const quantity = quantityIn.value //quantity from here
    quantityIn.value = ""

    const priceIn = document.querySelector('#price')
    const price = priceIn.value //price from here
    priceIn.value = ""

    const amountIn = document.querySelector('#amount')
    const amount = amountIn.value //amount from here
    amountIn.value = ""

    //Fetch call from here data submit part1
    fetch('http://localhost:5000/insert', {
        headers: {
            'Content-type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({ 
            invoiceNo: invoiceNo,
            customerName: customerName,
            productName: productName,
            description: description,
            quantity: quantity,
            price: price,
            amount: amount
        })
    })
    .then(response => response.json())
    .then(data => insertRowIntoTable(data['data']))
}

function insertRowIntoTable(data) {
    //console.log(data)
    const table = document.querySelector('table tbody')
    const isTableData = table.querySelector('.no-data')

    let tableHtml = "<tr>"

    for(var key in data) {
        if(data.hasOwnProperty(key)) {
            if(key === "dateAdded") {
                data[key] = new Date(data[key].toLocaleString())
            }
            tableHtml += `<td>${data[key]}</td>`
        }
    }
    tableHtml += `<td><button class="edit-row-btn" data-id=${data.id}>edit</button></td>`
    tableHtml += `<td><button class="delete-row-btn" data-id=${data.id}>delete</button></td>`
    tableHtml += `<td><button class="show-row-btn">show</button></td>`

    tableHtml += '</tr>'

    if(isTableData) {
        table.innerHTML = tableHtml
    }
    else {
        const newRow = table.insertRow();
        newRow.innerHTML = tableHtml
    }
}

function loadHTMLTable(data) {
    const table = document.querySelector('table tbody')
    if(data.length === 0) {
        table.innerHTML = "<tr><td class='no-data center-align' colspan='6'>No Order Found</td></tr>";
        return
    }

    let tableHtml = ""
    data.forEach(({ id, invoice_no, date_added, name }) => {
        tableHtml += "<tr>"
        tableHtml += `<td>${invoice_no}</td>`
        tableHtml += `<td>${new Date(date_added).toLocaleString()}</td>`
        tableHtml += `<td>${name}</td>`
        tableHtml += `<td><button class="edit-row-btn" data-id=${id}>edit</button></td>`
        tableHtml += `<td><button class="delete-row-btn" data-id=${id}>delete</button></td>`
        tableHtml += `<td><button class="show-row-btn">show</button></td>`
        tableHtml += "<tr>"
    });

    table.innerHTML = tableHtml
}