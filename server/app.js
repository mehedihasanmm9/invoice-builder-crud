const express = require('express')
const app = express()
const cors = require('cors')
const dotenv = require('dotenv')
dotenv.config()

const dbService = require('./dbService')
//const DbService = require('./dbService')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended:false }))

//create
app.post('/insert', (request, response) => {
    //console.log(request.body)
    const {
      invoiceNo,
      customerName,
      productName,
      description,
      quantity,
      price,
      amount,
    } = request.body;
    //console.log(request.body)
    const db = dbService.getDbServiceInstance()

    //First Insertion into table
    const result = db.insertNewData(invoiceNo, customerName)
    result
    .then(data => response.json({data: data}))
    .catch(err => console.log(err))

    //Second Insertion into table
    const result1 = db.insertNewData1(invoiceNo, productName, description, quantity, price, amount)
    result1
    .then(data => response.json({ success: true }))
    .catch(err => console.log(err))
})
//read
app.get('/getAll', (request, response) => {

    const db = dbService.getDbServiceInstance()

    const result = db.getAllData()
    result
    .then(data => response.json({data: data}))
    .catch(err => console.log(err))
    
})
//update

//delete

app.listen(process.env.PORT, () => console.log("App is running on " + process.env.PORT))