var mysql = require('mysql');
var dotenv = require('dotenv')
let instance = null
dotenv.config()

const connection = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USERNAME,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  port: process.env.DB_PORT 
});

connection.connect((err) => {
  if (err) {
    console.log(err.message)
  }
  console.log('db ' + connection.state)
});

class DBService {
  static getDbServiceInstance() {
    return instance ? instance : new DBService();
  }

  async getAllData() {
    try {
      const response = await new Promise((resolve, reject) => {
        const query = "SELECT * FROM names;";

        connection.query(query, (err, results) => {
          if (err) reject(new Error(err.message));
          resolve(results);
        });
      });
      //console.log(response);
      return response;
    } catch (error) {
      console.log(error);
    }
  }
  //First Insert query on the Name table
  async insertNewData(invoiceNo, customerName) {
    try {
      const dateAdded = new Date();
      const insertId = await new Promise((resolve, reject) => {
        const query =
          "INSERT INTO names (invoice_no, date_added, name) VALUES (?, ?, ?);";
        connection.query(
          query,
          [invoiceNo, dateAdded, customerName],
          (err, result) => {
            if (err) reject(new Error(err.message));
            resolve(result.insertId);
          }
        );
      });
      //console.log(insertId)
      return {
        //id: insertId,
        invoiceNo: invoiceNo,
        dateAdded: dateAdded,
        customerName: customerName
      };
    } catch (error) {
      console.log(error);
    }
  }

  //2nd query on the customers table
  async insertNewData1(
    invoiceNo,
    productName,
    description,
    quantity,
    price,
    amount
  ) {
    try {
      //const dateAdded = new Date()
      const insertId1 = await new Promise((resolve, reject) => {
        const query =
          "INSERT INTO customers (invoice_no, product, description, quantity, price, amount) VALUES (?, ?, ?, ?, ?, ?);";
        connection.query(
          query,
          [invoiceNo, productName, description, quantity, price, amount],
          (err, result) => {
            if (err) reject(new Error(err.message));
            resolve(result);
          }
        );
      });
      //console.log(insertId1)
      return insertId1;
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = DBService